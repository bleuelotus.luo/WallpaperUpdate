#! /bin/bash

varDate=$(date +%F)

if [ ! -d "$varDate" ]; then
  sudo mkdir $varDate
  echo "$varDate folder created"
else
  echo "$varDate folder exist"
fi
cd $varDate

if [ ! -d "Test" ]; then
  sudo mkdir Test
  echo "Test folder created"
else
  echo "Test folder exist" 
fi
cd Test

if [ ! -d "log" ]; then
  sudo mkdir log 
  echo "log folder created"
else
  echo "log folder exist" 
fi
cd log

if [ ! -f "OriginalInfo.log" ]; then
  sudo touch OriginalInfo.log
  echo "OriginalInfo.log created"
else
  echo "OriginalInfo.log exist" 
fi

if [ ! -f "DebugInfo.log" ]; then
  sudo touch DebugInfo.log
  echo "DebugInfo.log created"
else
  echo "DebugInfo.log exist" 
fi

if [ ! -f "SLAMInfo.log" ]; then
  sudo touch SLAMInfo.log
  echo "SLAMInfo.log created"
else
  echo "SLAMInfo.log exist" 
fi

